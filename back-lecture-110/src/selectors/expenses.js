import moment from 'moment';

// Get visible expenses
export default (expenses, { text, sortBy, startDate, endDate }) => {
    return expenses.filter((expense) => {
        const createdAtMoment = moment(expense.createdAt);
        const startDateMatch = startDate ? startDate.isSameOrBefore(createdAtMoment, 'day') : true;
        const endDateMatch = endDate ? endDate.isSameOrAfter(createdAtMoment, 'day') : true;
        const textMatch = typeof text !== 'string'
            || (typeof expense.note === 'string' && expense.note.toLowerCase().includes(text.toLowerCase()))
            || (typeof expense.description === 'string' && expense.description.toLowerCase().includes(text.toLowerCase()))
            ;
        return startDateMatch && endDateMatch && textMatch;
    }).sort((left, right) => {
        if (sortBy === 'date') {
            // show most recent first
            return left.createdAt < right.createdAt ? 1 : -1;
        } else if (sortBy === 'amount') {
            // show most expensive first
            return left.amount < right.amount ? 1 : -1;
        }
        return 0;
    });
};
