import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import AppRouter from './routers/AppRouter';
import configureStore from './store/configureStore';
import { addExpense } from './actions/expenses';
import { setTextFilter } from './actions/filters';
import getVisibleExpenses from './selectors/expenses';
import 'normalize.css/normalize.css';
import './styles/styles.scss';


const store = configureStore();
// store.subscribe(() => {
//     // const state = store.getState();
//     // const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);
//     // console.log("VISIBLE-EXPS", visibleExpenses);
//     console.log("SUBSCRIBE", store.getState());

// });

console.log(store.getState());

const expenseOne = store.dispatch(addExpense({ description: 'Water Bill', amount: 4500, createdAt: 4500 }));
const expenseTwo = store.dispatch(addExpense({ description: 'Gas Bill', amount: 1000, createdAt: 1000 }));
const expenseThree = store.dispatch(addExpense({ description: 'Rent', amount: 109500 }));
// store.dispatch(setTextFilter('bill'));
// store.dispatch(setTextFilter('water'));

// setTimeout(()=>{store.dispatch(setTextFilter('bill'));},3000);
const state = store.getState();
const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);
console.log("in apps.js VISIBLE-EXPS", visibleExpenses);
// ReactDOM.render(<AppRouter />, document.getElementById('app'));

const jsx = (

    <Provider store={store}>
        <AppRouter />
    </Provider>

);
ReactDOM.render(jsx, document.getElementById('app'));
