// console.log('destructuring');
// const person = {
//     name: 'John',
//     age: 23,
//     location: {
//         // temp: 92,
//         city: 'Tucson'
//     }
// };
// const {name, age} = person;
// const {city, temp:temperature=47} = person.location;
// console.log('yes');
// console.log(`${person.name} is ${person.age}.`);
// console.log(`It is ${temperature} in ${city}.`);

// const book = {
//     title: 'Ego is the Enemy',
//     author: 'Ryan Holiday',
//     publisher: {
//         name: 'Penguin'
//     }
// };
// const {name:publisherName = 'Self-Published'} = book.publisher;

// console.log(publisherName);


const address = ['1299 S. Juniper Street', 'Philadelphia','PA','19247'];
const [streetAddress, city, state = 'New York', zip] = address;

console.log(`You are in ${address[1]} ${address[2]}.`);
console.log(`You are on ${streetAddress} - ${city}, ${state} with ${zip}.`);

const item = ['Coffee (hot)', '$2.00', '$2.50', '$2.75'];
const [itemName,, mediumPrice] = item;
console.log(`A medium ${itemName} costs ${mediumPrice}`);