// https://codepen.io/gaearon/pen/gwoJZk?editors=0010#0
function tick() {
    const element = (
    <p>Howdy {new Date().toLocaleTimeString()}</p>
);

    ReactDOM.render(element,document.getElementById('app'));
}

setInterval(tick, 1000);