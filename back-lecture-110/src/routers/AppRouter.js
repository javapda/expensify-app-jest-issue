import React from 'react';
import { BrowserRouter, NavLink, Route, Switch } from 'react-router-dom';
import AboutPage from '../components/AboutPage';
import AddExpensePage from '../components/AddExpensePage';
import EditExpensePage from '../components/EditExpensePage';
import ExpenseDashboardPage from '../components/ExpenseDashboardPage';
import Header from '../components/Header';
import HelpPage from '../components/HelpPage';
import NotFoundPage from '../components/NotFoundPage';

const AppRouter = () => (
    <BrowserRouter>
        <div>

            <Header />
            <Switch>
                <Route exact={true} path="/" component={ExpenseDashboardPage} />
                <Route exact={false} path="/create" component={AddExpensePage} />
                <Route path="/edit/:id" component={EditExpensePage} />
                <Route exact={true} path="/help" component={HelpPage} />
                <Route exact={true} path="/about" component={AboutPage} />
                <Route component={NotFoundPage} />
            </Switch>
        </div>
    </BrowserRouter>

);

export default AppRouter;