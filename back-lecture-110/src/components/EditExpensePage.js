import React from 'react';
import { connect } from 'react-redux';
import ExpenseForm from './ExpenseForm';
import { editExpense, removeExpense } from '../actions/expenses';

const EditExpensePage = (props) => {
    console.log("PROPS:", props);
    console.log("history", props.history);
    console.log("location", props.location);
    console.log("match", props.match);
    return (
        <div>
            OK Edit Expense with id of {props.match.params.id}
            <ExpenseForm
                expense={props.expense}
                onSubmit={(expense) => {
                    // Dispatch the action to edit the expense
                    // Redirect to the dashboard
                    console.log('updated', expense);
                    props.dispatch(editExpense(props.expense.id, expense));
                    props.history.push('/');
                }}
            />
            <button onClick={() => {
                console.log('onClick in EditExpensePage');
                console.log(`props.expense.id=${props.expense.id}`);
                props.dispatch(removeExpense({ id: props.expense.id }));
                props.history.push('/');
            }
            }>remove</button>
        </div>
    );
};

const mapStateToProps = (state, props) => {
    return {
        expense: state.expenses.find((expense) => expense.id === props.match.params.id)
    };
};
export default connect(mapStateToProps)(EditExpensePage);