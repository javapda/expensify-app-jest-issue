
const path = require('path');

console.log('__dirname: ' + __dirname);
console.log(path.join(__dirname, "public"));

module.exports = {
    mode: 'development',
    // mode: 'production', // 'production' || 'development' || 'none'
    // entry: './src/playground/destructuring.js',
    // entry: './src/playground/redux-expensify.js',
    // entry: './src/playground/redux-101.js',
    // entry: './src/playground/hoc.js',
    entry: './src/app.js',
    output: {
        path: path.join(__dirname, "public"),
        // path: path.resolve(__dirname, "dist"),
        filename: 'bundle.js'
    },
    module: {
        rules: [{
            loader: 'babel-loader',
            test: /\.js$/,
            exclude: /node_modules/
        },
        {
            test: /\.s?css$/,
            use: [
                'style-loader',
                'css-loader',
                'sass-loader'
            ]
        }
        ]
    },
    // https://webpack.js.org/configuration/devtool/#devtool
    devtool: 'cheap-module-eval-source-map',

    // https://webpack.js.org/configuration/dev-server/#devserver-host
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        historyApiFallback: true,
        host: "0.0.0.0", // all hosts
        port: 8686
    }
};