# README #

when running 'yarn test' getting:

```
expensify-app yarn test       
yarn run v1.6.0
$ jest
jest-haste-map: @providesModule naming collision:
  Duplicate module name: expensify
  Paths: /Users/jkroub/react-course-projects/expensify-app/package.json collides with /Users/jkroub/react-course-projects/expensify-app/back-lecture-110/package.json
This warning is caused by a @providesModule declaration with the same name across two different files.
No tests found
In /Users/jkroub/react-course-projects/expensify-app
  52 files checked.
  testMatch: **/__tests__/**/*.js?(x),**/?(*.)+(spec|test).js?(x) - 0 matches
  testPathIgnorePatterns: /node_modules/ - 52 matches
Pattern:  - 0 matches
error Command failed with exit code 1.
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
```
